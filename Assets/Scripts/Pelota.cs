﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pelota : MonoBehaviour {

	public int puntos = 0;
	bool enJuego = false;
	public GameObject itemPrefab;
	public Text textoPuntos;
	
	public float velocidad = 8; //velocidad jugador
	public float limite; //valor absoluto del limite


	private int ladrillosRestantes;

	void Start (){
		ladrillosRestantes = GameObject.FindObjectsOfType<Ladrillo> ().Length; //contar todo los objetos de un tipo
	}


	void Update () {

		float desplazamiento = Input.GetAxisRaw ("Horizontal")*Time.deltaTime*velocidad;
		float posActual = transform.position.x;
		float posFinal = posActual+desplazamiento;
		
		if (posFinal >- limite && posFinal < limite && !enJuego) {
			
			transform.Translate (desplazamiento, 0, 0);
		}
	
		// Recogemos el Input
		bool saque = Input.GetButtonDown ("Jump");

		// Si hemos pulsado espacio
		if (saque==true) {

			// Si no estaba en juego (variable = false)
			if (enJuego == false) {

				// Cambiamos la variable para que no entre mas en este IF
				enJuego = true;

				// Añadimos la fuerza
				Rigidbody rigidbodyPelota = GetComponent<Rigidbody> ();
				rigidbodyPelota.AddForce (5, 5, 0, ForceMode.Impulse);
			}
		}

		Derrota ();
	}
	
	void OnCollisionEnter (Collision objetoChoque) {

		Rigidbody rigidbodyPelota = GetComponent<Rigidbody> ();

		if (Mathf.Abs (rigidbodyPelota.velocity.y) < 2) {
			Vector3 velocidad = rigidbodyPelota.velocity;
			velocidad.y = 5 * Mathf.Sign (rigidbodyPelota.velocity.y);
			rigidbodyPelota.velocity = velocidad;
		}


		Vector3 posicionLadrilloChocado = objetoChoque.gameObject.transform.position;
		Quaternion rotacionCapsula = Quaternion.identity;

		int numeroAleatorio = Random.Range (0, 100);
		
		if (objetoChoque.gameObject.tag == "Diez") {
			Destroy (objetoChoque.gameObject);
			ladrillosRestantes --;
			puntos += 10;
			print (puntos); 
			textoPuntos.text = puntos.ToString ();				
		

			if (numeroAleatorio < 10) {
				Instantiate (itemPrefab, posicionLadrilloChocado, rotacionCapsula);
			}
		}
		//Diez puntos

		if (objetoChoque.gameObject.tag == "Veinte") {
			Destroy (objetoChoque.gameObject);
			ladrillosRestantes --;
			puntos += 20;
			textoPuntos.text = puntos.ToString ();				
		
			if (numeroAleatorio < 10) {
				Instantiate (itemPrefab, posicionLadrilloChocado, rotacionCapsula);
			}
		}
		//Veinte puntos

		if (objetoChoque.gameObject.tag == "Cincuenta") {
			Destroy (objetoChoque.gameObject);
			ladrillosRestantes --;
			puntos += 50;
			print (puntos);
			//textoPuntos.text = puntos.ToString ();				

		
			if (numeroAleatorio < 10) {
				Instantiate (itemPrefab, posicionLadrilloChocado, rotacionCapsula);
			}
		}
		//Cincuenta puntos

		if (objetoChoque.gameObject.tag == "Cien") {
			Destroy (objetoChoque.gameObject);
			ladrillosRestantes --;
			puntos += 100;
			print (puntos);
			//textoPuntos.text = puntos.ToString ();				
		
		
			if (numeroAleatorio < 10) {
				Instantiate (itemPrefab, posicionLadrilloChocado, rotacionCapsula);
			}
		}
		//Cien puntos

		if (objetoChoque.gameObject.tag == "Metalico") {
			Ladrillo scriptLadrillo = objetoChoque.gameObject.GetComponent<Ladrillo> ();
			if (scriptLadrillo.GolpesRecibidos == 0) {
				scriptLadrillo.GolpesRecibidos += 1;

				Renderer componenteRenderer = objetoChoque.gameObject.GetComponent<Renderer> ();
				componenteRenderer.material.color = Color.grey;
			} else {
				if (scriptLadrillo.GolpesRecibidos >= 1) {
					Destroy (objetoChoque.gameObject);
					ladrillosRestantes --;

					puntos += 200;
					textoPuntos.text = puntos.ToString ();


					if (numeroAleatorio < 10) {
						Instantiate (itemPrefab, posicionLadrilloChocado, rotacionCapsula);
					}
				}
			}
		}
		if (ladrillosRestantes <= 0) {
			Application.LoadLevel (Application.loadedLevel + 1);
		}
	}
	void Derrota(){
		
		if (transform.position.y < -5) {
			
			Application.LoadLevel ("Game Over");
		}
	}	

}