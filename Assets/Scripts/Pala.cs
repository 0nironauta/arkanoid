﻿using UnityEngine;
using System.Collections;

public class Pala : MonoBehaviour {

	public float velocidad = 8; //velocidad jugador
	public float limite; //valor absoluto del limite
	public Pelota scriptPelota;

	void Update () { //en cada frame
	
		float desplazamiento = Input.GetAxisRaw ("Horizontal")*Time.deltaTime*velocidad;
		float posActual = transform.position.x;
		float posFinal = posActual+desplazamiento;

		if (posFinal >- limite && posFinal < limite) {

			transform.Translate (desplazamiento, 0, 0);
		}
	}
	void OnTriggerEnter (Collider otroObjeto){

		if (otroObjeto.gameObject.tag == "Item"){
			Destroy (otroObjeto.gameObject);
			scriptPelota.puntos += 100;
			scriptPelota.textoPuntos.text = scriptPelota.puntos.ToString ();
		}
	}

	void OnCollisionEnter (Collision otro){

		float inputH = Input.GetAxisRaw ("Horizontal");

		if (inputH != 0) {
			otro.rigidbody.velocity = Vector3.zero;
			otro.rigidbody.AddForce (inputH * 5, 5, 0, ForceMode.VelocityChange);
		}
	}
}
